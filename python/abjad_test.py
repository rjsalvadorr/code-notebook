from abjad import *

staff = Staff("c'4 d'4 e'4 f'4 c'4 d'4 e'4 f'4 c'4 d'4 e'4 f'4 c'4 d'4 e'4 f'4 c'4 d'4 e'4 f'4")

lilyBlock1 = Score([staff])
lilyBlock2 = Score([staff])
lilyBlock3 = Score()

tBlock = lilypondfiletools.Block(name='score')
tMarkup = Markup('whoa what')
tBlock.items.append(tMarkup)
tBlock.items.append(staff)

print(format(tBlock))

lilypond_file = lilypondfiletools.LilyPondFile.new(
    lilyBlock1,
    global_staff_size=20,
    default_paper_size=('letter', 'portrait'),
    )

lilypond_file.header_block.title = markuptools.Markup('Something Something')
lilypond_file.header_block.subtitle = markuptools.Markup('More things')
lilypond_file.header_block.composer = markuptools.Markup('Who dat?')


# Writing results to file
targetFile = open('abjad_test.ly', 'w')
targetFile.write(format(lilypond_file))
show(lilypond_file)

"""
Write to a lilypond file with those blocks, instead of using the lilypond_file itself.
The constructor from lilypondfiletools limits us to only one score object.
We'll have to use several score objects, like at:
http://lilypond.org/doc/v2.19/Documentation/notation/multiple-scores-in-a-book
"""
