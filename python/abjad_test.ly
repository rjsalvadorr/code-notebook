% 2017-05-11 21:20

\version "2.18.2"
\language "english"

#(set-default-paper-size "letter" 'portrait)
#(set-global-staff-size 20)

\header {
    composer = \markup { "Who dat?" }
    subtitle = \markup { "More things" }
    title = \markup { "Something Something" }
}

\layout {}

\paper {}

\score {
    \new Score <<
        \new Score <<
            \new Staff {
                c'4
                d'4
                e'4
                f'4
                c'4
                d'4
                e'4
                f'4
                c'4
                d'4
                e'4
                f'4
                c'4
                d'4
                e'4
                f'4
                c'4
                d'4
                e'4
                f'4
            }
        >>
    >>
}