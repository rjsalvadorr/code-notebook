// Class/module for finding possible keys based on chord symbols.

class KeyFinder {
    findKeys(chordSequence) {
        /** 
         * Find the key(s) for the given chord sequence.
         * Returns an object like:
         * 
         * {
         *   G major: '1-34',
         *   D major: '35-65'
         * }
         * 
         * Where the numbers represent where in the sequence each key starts.
         */
        return null;
    }

    findPossibleKeys(chordSequence) {
        /**
         * Finds the possible keys for a given chord sequence.
         * Returns an object like:
         * 
         * {
         *   G major: 20,
         *   D major: 12,
         *   B minor: 5
         * }
         * 
         * Where the numbers represent a score, showing how well those chords fit in each key.
         * I suppose we can use certain movements like "ii-V-I" or "IV-V-I" to influence the score.
         */
        return null;
    }
}