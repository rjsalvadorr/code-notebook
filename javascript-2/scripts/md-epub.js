'use strict';

const OUTPUT_FILE = 'G:\\Code\\Web Projects\\code-sketches\\javascript-modern\\scripts\\test\\test.epub';
const INPUT_FILE = 'G:\\Code\\Web Projects\\code-sketches\\javascript-modern\\scripts\\test\\test.md';

const { spawn } = require('child_process');
const ls = spawn('pandoc', ['-o', OUTPUT_FILE, INPUT_FILE, '--toc', '--toc-depth=2']);

ls.stdout.on('data', data => {
  console.log(`stdout: ${data}`);
});

ls.stderr.on('data', data => {
  console.log(`stderr: ${data}`);
});

ls.on('close', code => {
  if(code != 0) {
    console.log(`child process exited with code ${code}`);
  }
});
