---
title: CBT - Cognitive Distortions
author: RJ Salvador
rights:  Creative Commons Non-Commercial Share Alike 3.0
language: en-US
...

# CBT: Cognitive Distortions

_Specific mental distortions that the mind continuously coughs up_

In Dr. Reichmann's system, automatic thoughts are frequently distorted in certain ways. Many of these recurring distortions follow these patterns:

1. All-or-nothing Thinking
1. Overgeneralization
1. Mental Filter
1. Disqualifying the positive
1. Jumping to Conclusions
1. Magnification (Catastrophizing) or Minimization
1. Emotional Reasoning
1. Should Statements
1. Labelling and Mislabelling
1. Personalization

&nbsp;

---

## 1. All-or-nothing Thinking

You see things in black and white categories. If your performance falls short of perfect, you see yourself as a total failure.

&nbsp;

## 2. Overgeneralization

You see a single negative event as a never-ending pattern.

- “_This shit always happens to me._”
- “_Of course, good things would never happen to me._”

&nbsp;

## 3. Mental Filter

You pick out a single negative detail and dwell on it exclusively, so that your vision of all reality becomes darkened, like the drop of ink that discolours the entire beaker of water.

&nbsp;

## 4. Disqualifying the positive

You reject positive experiences by insisting they “don’t count” for some reason or other. In this way you can maintain a negative belief that is contradicted by your everyday experiences.

&nbsp;

## 5. Jumping to Conclusions

You make a negative interpretation even though there are no definite facts that convincingly support your conclusion.

**a. Mind Reading**: You arbitrarily conclude that someone is reacting negatively to you, and you don’t bother to check this out.

**b. Fortune-Telling**: You anticipate that things will turn out badly, and you feel convinced that your prediction is an already established fact.

&nbsp;

## 6. Magnification (Catastrophizing) or Minimization

You exaggerate the importance of things _(such as your goof-up or someone else’s achievement)_, or you inappropriately shrink things until they appear tiny _(your own desirable qualities or other peoples’ imperfections)_. This is also called the “binocular trick”.

&nbsp;

## 7. Emotional Reasoning

You assume that your negative emotions necessarily reflect the way things really are: “I feel it, therefore it must be true”.

&nbsp;

## 8. Should Statements

You try to motivate yourself with _"shoulds"_ and _"shouldnt’s"_ as if you had to be whipped and punished before you could be expected to do anything. _“Musts”_ and _“oughts”_ are also offenders. The emotional consequence is guilt. When you direct _“should”_ statements to others, you feel anger, frustration, and resentment.

&nbsp;

## 9. Labelling and Mislabelling

This is an extreme form of overgeneralization. Instead of describing your error, you attach a negative label to yourself. “I’m a loser.” When someone else’s behaviour rubs you the wrong way, you attach a negative label to him/her. “He’s an idiot.” Mislabelling involves describing an event with language that is highly coloured and emotionally loaded.

&nbsp;

## 10. Personalization

You see yourself as the cause of some negative external event for which, in fact, you were not primarily responsible.

&nbsp;
