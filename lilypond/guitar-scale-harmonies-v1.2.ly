\version "2.18.2"
\language "english"


\header {
    maintainer = "RJ Salvador"
    maintainerEmail = "randolph.salvador@gmail.com"
    % copyright = \markup { \abs-fontsize #8  "v1.2" }
    tagline = \markup { \abs-fontsize #12 "© RJ Salvador, 2018" }
}

guitarNotationGlobal = {
  \relative
  \clef "treble_8"
}

guitarTabGlobal = {
  \relative
  \time 4/4
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

guitarNotation = {
  \relative
  \clef "treble_8"
  \time 8/4
  
  \key c \major
  <e g c'>4^\markup {
    \column {
      \line { \huge "C Major" }
      \line { Scale starting on B string}
    }
  } d' <g c' e'> f'
  <c' e' g'> a' b' <e' g' c''>
  \bar "|." \break
  
  
  \key d \minor
  <f a d'>4^\markup {
    \column {
      \line { \huge "D minor" }
      \line { Scale starting on B string}
    }
  } e' <a d' f'> g'
  <d' f' a'> bf' c'' <f' a' d''>
  \bar "|." \break
  
  %%%%%
  
  \set Staff.explicitKeySignatureVisibility = #all-invisible
  \override Staff.KeyCancellation.break-visibility = #all-invisible
  \key c \major
  <e g c'>4^\markup {
    \column {
      \line { \huge "C Major" }
      \line { Scale starting on G string}
    }
  } d' <g c' e'> f'
  <c' e' g'> a' b' <e' g' c''>
  \bar "|." \break
  
  
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
  \key a \minor
  <c e a>4^\markup {
    \column {
      \line { \huge "A minor" }
      \line { Scale starting on G string}
    }
  } b <e a c'> d'
  <a c' e'> f' g' <c' e' a'>
  \bar "|." \break
  
  %%%%%
  
  \key f \major
  <a, c f>4^\markup {
    \column {
      \line { \huge "F Major" }
      \line { Scale starting on D string}
    }
  } g <c f a> bf
  <f a c'> d' e' <a c' f'>
  \bar "|." \break
  
  
  \key e \minor
  <g, b, e>4^\markup {
    \column {
      \line { \huge "E minor" }
      \line { Scale starting on D string}
    }
  } fs <b, e g> a
  <e g b> c' d' <g b e'>
  \bar "|."
}



guitarTab = {
  \relative
  \time 8/4
  
  
  \key c \major
  <e g c'>4 d' <g c' e'\2> f'
  <c' e' g'> a' b' <e' g' c''>
  \bar "|." \break
  
  
  \key d \minor
  <f a d'>4 e'\2 <a d' f'\2> g'
  <d' f' a'> bf' c'' <f' a' d''>
  \bar "|." \break
  
  %%%%%
  
  \key c \major
  <e g c'\3>4 d'\3 <g c' e'\2> f'\2
  <c' e'\3 g'\2> a' b' <e' g' c''>
  \bar "|." \break
  
  
  \key a \minor
  <c e a>4 b\3 <e a c'\3> d'
  <a c' e'\2> f'\2 g' <c' e' a'>
  \bar "|." \break
  
  %%%%%
  
  \key f \major
  <a, c f>4 g\4 <c f a> bf
  <f a c'\3> d' e'\2 <a c' f'\2>
  \bar "|." \break
  
  
  \key e \minor
  <g, b, e>4 fs <b, e g\4> a
  <e g b\3> c'\3 d' <g b e'\2>
  \bar "|."
}



pieceOne = {
  \clef "treble_8"
  \time 3/4
  \key c \major
  \tempo "Moderato" 4 = 110 - 115
  
  e8 g c' d' c' b
  
  c'4 d' e'
  
  <g c' e'>2.
  
  <g c' d'>2. \break
  
  %%%%%
  
  a8 c' f' g' f' e'
  
  f'4 g' a'
  
  <<
    {
      \voiceOne
      a'4 b' a'
    }
    \new Voice {
      \voiceTwo
      <c' f'>2.
    }
  >> \oneVoice
  
  <c' e' g'>2.\break
  
  %%%%%
  
  e8 g c' d' c' b
  
  c'4 d' e'
  
  <g c' e'>2.
  
  <<
    {
      \voiceOne
      d'2 e'4
    }
    \new Voice {
      \voiceTwo
      <g c'>2.
    }
  >> \oneVoice \break
  
  %%%%%
  
  <<
    {
      \voiceOne
      f'4 e' d'
      
      e'4 g c'
      
      d'4 g b
    }
    \new Voice {
      \voiceTwo
      <a c'>4 r2
      
      <g c'>2.
      
      <g b>2.
    }
  >> \oneVoice
  
  <e g c'>2.
}



pieceOneTab = {
  \time 3/4
  \key c \major
  
  e8 g c' d' c' b
  
  c'4 d' e'\2
  
  <g c' e'\2>2.
  
  <g c' d'>2. \break
  
  %%%%%
  
  a8 c' f' g' f' e'
  
  f'4 g' a'
  
  <c' f' a'>4 b' a'
  
  <c' e' g'>2.\break
  
  %%%%%
  
  e8 g c' d' c' b
  
  c'4 d' e'\2
  
  <g c' e'\2>2.
  
  <g c' d'>2 e'4\2 \break
  
  %%%%%
  
  <a c' f'\2>4 e'\2 d'
  
  <g c' e'\2>4 g\4 c'\3
  
  <g b d'\2>4 g\4 b\3
  
  <e g c'>2. \bar "|."
}



arpeggiosContextInNotation = {
  \relative
  \clef "treble_8"
  \time 4/4
  \key g \major
  
  g,8^\markup {
    \column {
      \line { "G Major (I)" }
    }
  } b, d g b d' g'4
  
  g'8 d' b g d b, g,4 \break
  
  e8^\markup {
    \column {
      \line { "E minor (vi)" }
    }
  } g b e' g' b' e''4
  
  e''8 b' g' e' b g e4 \break
  
  a,8^\markup {
    \column {
      \line { "A minor (ii)" }
    }
  } c e a c' e' a'4
  
  a'8 e' c' a e c a,4 \break
  
  d8^\markup {
    \column {
      \line { "D major (V)" }
    }
  } fs a d' fs' a' d''4
  
  d''8 a' fs' d' a fs d4 \bar "|."
}



arpeggiosContextInTab = {
  \relative
  \time 4/4
  \key g \major
  
  g,8 b, d\5 g\4 b\3 d'\3 g'4\2
  
  g'8\2 d'\3 b\3 g\4 d\5 b, g,4 \break
  
  e8\5 g\5 b\4 e'\3 g'\3 b'\2 e''4
  
  e''8 b'\2 g'\3 e'\3 b\4 g\5 e4\5 \break
  
  a,8\6 c\6 e\5 a\4 c'\4 e'\3 a'4\2
  
  a'8\2 e'\3 c'\4 a\4 e\5 c\6 a,4\6 \break
  
  d8\5 fs\4 a\4 d'\3 fs'\2 a'\2 d''4
  
  d''8 a'\2 fs'\2 d'\3 a\4 fs\4 d4\5 \bar "|."
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% SCORE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(set-global-staff-size 26)

\bookpart {
  
  \header {
    title = "GUITAR EXERCISES"
    subtitle = "Opus 6"
    subsubtitle = "2018"
  }
  
  \markup \abs-fontsize #14 {
    \column {
      \hspace #0
      \hspace #0
      \wordwrap {
        A collection of classical guitar exercises assembled by a mostly self-taught guitarist.
        It serves mostly as my guitar knowledge journal,
        but I'm sure it could be useful to other guitarists seeking to learn.
        I hope it helps you as much as it helped me.
      }
      \hspace #0
      \hspace #0
      "RJ Salvador"
      rjsalvadorr@gmail.com
      www.rj-salvador.com
      \hspace #0
      \hspace #0
      \wordwrap {
        At the time of writing, I had one main goal in music: to improvise as fluently as possible in whatever instrument I'm holding.
        These exercises work towards that goal, mainly by teaching skills for moving through the fretboard.
        With these skills, I wanted to:
      }
      \hspace #0
      \hspace #0
      "-  use the guitar's entire range in my improvisation and writing"
      "-  be comfortable in any position on the fretboard"
      "-  become more responsive and versatile in jam sessions"
      "-  avoid the problem of playing the same stuff over and over again"
      \hspace #0
    }
  }
}

%%%%%%%%%%  PART 1 - SCALES WITH HARMONY

\bookpart {
  
  \header {
    title = ##f
    subtitle = "1. Scales with harmony"
  }
  
  \markup {
    \column {
      \hspace #0
      \wordwrap \abs-fontsize #12 {
        These exercises were written for practicing chord inversions.
        The goal here is to memorize the chord patterns and fingerings.
        Once mastered, they can be used for any chord root.
        Be sure to go up and down the scale.
      }
      \hspace #0
      \hspace #0
    }
  }
  
  \score {
    
    \layout {
      \omit Voice.StringNumber
      indent = 0.0\cm
    }
    
    \new StaffGroup <<
      
      \new Staff <<
        \override Score.BarNumber.transparent = ##t
        \override Staff.TimeSignature.break-visibility = ##(#f #f #f)
        \override Staff.TimeSignature.transparent = ##t
        \set Staff.explicitKeySignatureVisibility = #end-of-line-invisible
        \set Staff.printKeyCancellation = ##f 
        \guitarNotation
      >>
       
      \new TabStaff <<
        \set Staff.explicitKeySignatureVisibility = #end-of-line-invisible
        \set Staff.printKeyCancellation = ##f 
        \set TabStaff.restrainOpenStrings = ##t
        \guitarTab
      >>
    >>
  }
}

\bookpart {
  
  \header {
    subtitle = "Estudio invertido"
    opus = "Op. 6, No. 1"
  }
  
  \markup {
    \column {
      \hspace #0
      \wordwrap \abs-fontsize #12 {
        A musical example showing how those chord voicings and inversions could be used.
      }
    }
  }
  
  \score {
    
    \layout {
      \omit Voice.StringNumber
      indent = 0.0\cm
    }
    
    \new StaffGroup <<
      
      \new Staff <<
        \pieceOne
      >>
       
      \new TabStaff <<
        \set TabStaff.restrainOpenStrings = ##t
        \pieceOneTab
      >>
    >>
  }
}

%%%%%%%%%%  PART 2 - ARPEGGIOS IN CONTEXT

\bookpart {
  
  \header {
    title = ##f
    subtitle = "2. Arpeggios in Context"
  }

  \markup {
    \column {
      \hspace #0
      \wordwrap \abs-fontsize #12 {
        This next exercise shows arpeggios in a harmonic context.
        This uses the "\"I−vi−ii−V\"" progression, which is common in jazz and popular music.
        As with the previous exercise, the goal here is to memorize the arpeggio patterns.
        The exercise also covers a lot of the fretboard, helping you gain familiarity with fretboard "\"geography\"".
      }
      \hspace #0
      \hspace #0
    }
  }
  
  \score {
    
    \layout {
      \omit Voice.StringNumber
      indent = 0.0\cm
    }
    
    \new StaffGroup <<
      
      \new Staff <<
        \override Score.BarNumber.transparent = ##t
        \arpeggiosContextInNotation
      >>
       
      \new TabStaff <<
        \set TabStaff.restrainOpenStrings = ##t
        \arpeggiosContextInTab
      >>
    >>
  }
}



\paper {
    #(set-paper-size "letter")
    top-margin = 0.66\in
    left-margin = 0.75\in
    right-margin = 0.75\in
    bottom-margin = 0.66\in

    print-page-number = true
}
