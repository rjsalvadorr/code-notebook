\header {
  title = "Hip-hop beat transcriptions"
  subtitle = "Das good mane"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gucci Mane - I Get The Bag
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

beatTempo = 133

hiHats = \drummode {
  hh8 hh hh hh hh hh hh hh
  hh hh hh hh hh hh r4
}

kickSnare = \drummode {
  bd4. bd8 sn4 r
  r4 bd sn r
}

\score {
  <<
    \new DrumStaff \with { instrumentName = #"Drums" }
    <<
      \tempo 4 = \beatTempo
      \new DrumVoice { \stemUp \hiHats }
      \new DrumVoice { \stemDown \kickSnare }
    >>
  >>
  \header {
    piece = "Gucci Mane - I Get The Bag"
  }
  \layout { }
  \midi { \tempo 4 = \beatTempo }
}
