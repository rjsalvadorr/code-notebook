# Code Sandbox

Keep handy code snippets here! Putting this in a repo lets you use the same snippets on all your devices.

## Python
For this little trial run, I've started with Python 3. We'll see if that bites me later on.

### Packages I'm playing with:

- [abjad](http://www.projectabjad.org/)
- [mingus](https://bspaans.github.io/python-mingus/)
- [music21](http://web.mit.edu/music21/)

### Virtual Environments
Fresh setup? On Python 3, run `python -m venv path/to/virtual/env`. In this case, our target folder is `python/` Here's what I did for Python 2: `G:\Programming\Web Projects\code-sketches\python>virtualenv -p C:\Python27\python.exe .`

On Windows, just run `python\Scripts\activate.bat` to activate the virtual env. Running `python\Scripts\deactivate.bat` will do the opposite.

For more info, [read the docs](https://docs.python.org/3/library/venv.html).

### Updating dependency list
From the python folder, just do `pip freeze > requirements.txt`. Not sure if the virtual env. has to be activated.
