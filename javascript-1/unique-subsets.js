var motherSet = [1, 2, 3, 4, 5, 6, 7, 8, 9];


// This solution is from
// http://stackoverflow.com/questions/20614876/find-all-unique-subsets-of-a-set-of-values

function subsets(arr, pos, depth, startPos) {

  if(pos === depth) {
    var outString = "";
    for(var i = 0; i < depth; i++) {
      outString += arr[i] + ' ';
    }
    console.log(outString);
    return;
  }

  for(var i = startPos; i < arr.length; i++) {

    // optimization - not enough elements left
    if(depth - pos + 1 > arr.length) {
      return;
    }

    // swap pos and i
    var temp = arr[pos];
    arr[pos] = arr[i];
    arr[i] = temp;

    subsets(arr, pos+1, depth, i + 1);

    // swap pos and i back - otherwise things just gets messed up
    temp = arr[pos];
    arr[pos] = arr[i];
    arr[i] = temp;
  }
}

// trying another solution from the
function permutations(arr, pos, depth) {

  if(pos === depth) {
    var outString = "";
    for(var i = 0; i < depth; i++) {
      outString += arr[i] + ' ';
    }
    console.log(outString);
    return;
  }

  for(var i = 0; i < arr.length; i++) {

    // optimization - not enough elements left
    if(depth - pos + 1 > arr.length) {
      return;
    }

    // swap pos and i
    var temp = arr[pos];
    arr[pos] = arr[i];
    arr[i] = temp;

    permutations(arr, pos + 1, depth);

    // swap pos and i back - otherwise things just gets messed up
    temp = arr[pos];
    arr[pos] = arr[i];
    arr[i] = temp;
  }
}

console.log('\nTESTING SUBSETS\n')

subsets(motherSet, 0, 4, 0);

//console.log('\nTESTING PERMUTATIONS\n')

//permutations(motherSet, 0, 4);
