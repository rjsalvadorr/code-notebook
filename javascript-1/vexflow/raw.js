window.$ = window.jQuery = require('jQuery');
var Vex = require('vexflow');

$(document).ready(function() {
  /**
  COMMENTING THIS OUT, SINCE IT LOOKS LIKE THE EASYSCORE API DOESN'T SUPPORT TEXT/CHORD SYMBOLS

  var vf = new Vex.Flow.Factory({
    renderer: {selector: 'vexflow-root', width: 500, height: 200}
  });

  var score = vf.EasyScore();
  var system = vf.System();

  system.addStave({
    voices: [
      score.voice(score.notes('C#5/q, B4, A4, G#4', {stem: 'up'})),
      score.voice(score.notes('C#4/h, C#4', {stem: 'down'}))
    ]
  }).addClef('treble').addTimeSignature('4/4');

  vf.draw();
  **/


  VF = Vex.Flow;

  // Create an SVG renderer and attach it to the DIV element named "boo".
  var div = document.getElementById("vexflow-root")
  var renderer = new VF.Renderer(div, VF.Renderer.Backends.SVG);

  // Configure the rendering context.
  renderer.resize(500, 500);
  var context = renderer.getContext();
  context.setFont("Arial", 14, "").setBackgroundFillStyle("#eed");

  // Create a stave of width 400 at position 10, 40 on the canvas.
  var stave = new VF.Stave(10, 40, 400);

  // Add a clef and time signature.
  stave.addClef("treble").addTimeSignature("4/4");

  // Connect it to the rendering context and draw!
  stave.setContext(context).draw();

  var notes = [];
  notes.push(new VF.StaveNote({clef: "treble", keys: ["c/4"], duration: "w" }).addAnnotation(0, new VF.Annotation('Am7')));
  notes.push(new VF.StaveNote({clef: "treble", keys: ["d/4"], duration: "w" }).addAnnotation(0, new VF.Annotation('D7')));
  notes.push(new VF.StaveNote({clef: "treble", keys: ["b/4"], duration: "wr" }));
  notes.push(new VF.StaveNote({clef: "treble", keys: ["c/4", "e/4", "g/4"], duration: "w" }));

  // Create a voice in 4/4 and add above notes
  var voice = new VF.Voice({num_beats: 4,  beat_value: 1});
  voice.addTickables(notes);

  // Format and justify the notes to 400 pixels.
  var formatter = new VF.Formatter().joinVoices([voice]).format([voice], 400);

  // Render voice
  voice.draw(context, stave);
});
