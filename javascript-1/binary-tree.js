// All this code is from
// https://www.syncano.io/blog/data-structures-in-javascript/

function Node(data, left, right) {
  this.data = data;
  this.left = left;
  this.right = right;
}

const prettyPrint = (cur) => {
  if(cur.left != null){
    prettyPrint(cur.left);
  }
  process.stdout.write(cur.data + ", ");
  if(cur.right != null){
    prettyPrint(cur.right);
  }
};

const append = (cur, data) => {
  if(data <= cur.data ) {
    if(cur.left == null) {
      cur.left = new Node(data, null, null);
    } else {
      append(cur.left,data);
    }
  } else {
    if(cur.right == null) {
      cur.right = new Node(data, null, null);
    } else {
      append(cur.right, data);
    }
  }
};

function BinaryTree() {
  this.head = new Node(null, null, null);
};

BinaryTree.prototype.prettyPrint = function() {
  const cur = this.head;
  prettyPrint(cur);
};

BinaryTree.prototype.append = function(data) {
  if(this.head.data == null ) {
    this.head = new Node(data,null,null);
  } else {
    const cur = this.head;
    append(cur,data);
  }
};

BinaryTree.prototype.remove = function(data) {
  return "unimplemented";
  // the code is ugly and long.
}

const tree = new BinaryTree();

tree.append(2);
tree.append(14);
tree.append(7);
tree.append(5);
tree.append(17);
tree.prettyPrint();
