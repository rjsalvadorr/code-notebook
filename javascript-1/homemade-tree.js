/*

Implement a custom tree with the following properties:

- Has a recursive generation function. Starting from the root node, each node will call a function on itself to generate child nodes. This happens until the tree is full. TODO: Find a good max limit.
- No append function. The tree is completely generated and initialized all in one go. No need to append anything to it.
- Read function that prints out every possible node combination. Basically all the straight paths, from the root to each leaf.

*/

function Node(data, left, right) {
  this.data = data;
  this.left = left;
  this.right = right;
};

function BinaryTree() {
  this.MAX_DEPTH = 5;
  this.START = 3;
  this.numNodes = 1;

  this.head = new Node(this.START, null, null);

  // generate the rest of the tree from here
  this._generateNodes(this.head, 1);
};

BinaryTree.prototype._generateNodes = function(currentNode, numDepth) {
  if(numDepth === this.MAX_DEPTH) {
    return;
  } else {
    var newDepth = numDepth + 1;
    var newDataL = currentNode.data * 2;
    var newDataR = currentNode.data * 3;

    currentNode.left = new Node(newDataL, null, null);
    currentNode.right = new Node(newDataR, null, null);

    console.log('Nodes generated!');
    this.numNodes += 2;

    this._generateNodes(currentNode.left, newDepth);
    this._generateNodes(currentNode.right, newDepth);
  }
};

BinaryTree.prototype._printNode = function(currentNode, numIndent, indentString) {
/*
  root
  -- left
    -- left
      -- left
      -- right
    -- right

  -- right
*/
//  var indentStringR = '';
  var newIndent = numIndent + 1;
  var newIndentStringL = indentString + '  │   ';
  var newIndentStringR = indentString + '      ';
/*
  // console.log('numIndent=' + numIndent);

  for(var i = 0; i < numIndent; i++) {
    indentStringL += ' │   ';
    indentStringR += ' │   ';
  }
*/
  var string = '[' + currentNode.data + ']';

  if(currentNode.left && currentNode.right) {
    string += '\n' + indentString + '  │';
    string += '\n' + indentString + '  ├-L-' + this._printNode(currentNode.left, newIndent, newIndentStringL);
    string += '\n' + indentString + '  │';
    string += '\n' + indentString + '  └-R-' + this._printNode(currentNode.right, newIndent, newIndentStringR);
  }

  return string;
};

BinaryTree.prototype.currentDepth = function() {
/*
  f(1) = 1
  f(2) = 2
  f(3) = 4
  f(4) = 8
  f(5) = 16
  f(6) = 32
  f(7) = 64
  And so on. Which makes f(x) = 2^(x - 1)
  The inverse of this would be... log2(x) + 1
*/

  var rawDepth = Math.log2(this.numNodes) + 1;
  var depth = Math.floor(rawDepth);

  console.log('Current Depth = ' + depth);

  return depth;
};

BinaryTree.prototype.print = function() {
  console.log('\n=============== TREE VIEW ===============\n');

  var outputText = this._printNode(this.head, 0, '');

  console.log(outputText);
  console.log('\nnumNodes = ' + this.numNodes);
};

var tree = new BinaryTree();
tree.print();
